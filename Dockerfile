FROM golang:1.17-alpine AS builder

WORKDIR /build

# Copy and download dependencies using go.mod
COPY . ./
RUN go mod download

# Set necessarry environment vairables and compile the app
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -v -ldflags="-s -w" -o quotaflag .

FROM scratch

LABEL org.opencontainers.image.authors="christian@roessner.email"
LABEL com.roessner-network-solutions.vendor="Rößner-Network-Solutions"
LABEL version="@@gittag@@-@@gitcommit@@"
LABEL description="Postfix policy service that blocks mails, if a users mailbox is over quota."

WORKDIR /usr/app

# Copy binary to destination image
COPY --from=builder ["/build/quotaflag", "./"]

EXPOSE 4648

CMD ["/usr/app/quotaflag", "server"]
