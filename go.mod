module quotaflag

go 1.17

require (
	github.com/akamensky/argparse v1.3.1
	github.com/go-ldap/ldap/v3 v3.4.1
	github.com/segmentio/ksuid v1.0.4
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20200615164410-66371956d46c // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.1 // indirect
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
)
