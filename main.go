/*
quotaflag
Copyright 2022 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

const version = "@@gittag@@-@@gitcommit@@"

var (
	ldapServer *LDAP

	DebugLogger *log.Logger
	InfoLogger  *log.Logger
	ErrorLogger *log.Logger
)

func init() {
	InfoLogger = log.New(os.Stdout, "INFO: ", 0)
	DebugLogger = log.New(os.Stdout, "DEBUG: ", log.Lshortfile)
	ErrorLogger = log.New(os.Stdout, "ERROR: ", log.Lshortfile)
}

func main() {
	var (
		err    error
		server net.Listener
	)

	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	cfg := new(CmdLineConfig)
	cfg.Init(os.Args)

	go func() {
		sig := <-sigs
		if cfg.VerboseLevel >= logLevelInfo {
			InfoLogger.Println("Shutting down. Received signal:", sig)
		}
		os.Exit(0)
	}()

	if cfg.CommandServer {
		if cfg.VerboseLevel >= logLevelInfo {
			InfoLogger.Printf("Starting quotaflag server (%s): '%s:%d'\n", version, cfg.ServerAddress, cfg.ServerPort)
		}

		if cfg.VerboseLevel == logLevelDebug {
			DebugLogger.Println(cfg)
		}

		if cfg.UseLDAP {
			ldapServer = &LDAP{
				ServerURIs:    cfg.LDAP.ServerURIs,
				BaseDN:        cfg.LDAP.BaseDN,
				BindDN:        cfg.LDAP.BindDN,
				BindPW:        cfg.LDAP.BindPW,
				Filter:        cfg.LDAP.Filter,
				ResultAttr:    cfg.LDAP.ResultAttr,
				StartTLS:      cfg.LDAP.StartTLS,
				TLSSkipVerify: cfg.LDAP.TLSSkipVerify,
				TLSCAFile:     cfg.LDAP.TLSCAFile,
				TLSClientCert: cfg.LDAP.TLSClientCert,
				TLSClientKey:  cfg.LDAP.TLSClientKey,
				SASLExternal:  cfg.LDAP.SASLExternal,
				Scope:         cfg.LDAP.Scope,
				Mu:            new(sync.Mutex),
			}
			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Println("LDAP-settings:", ldapServer)
			}
			if err := ldapServer.connect(cfg.VerboseLevel, "-"); err != nil {
				ErrorLogger.Fatalln(err)
			}
			ldapServer.bind(cfg.VerboseLevel, "-")
		}

		server, err = net.Listen("tcp", fmt.Sprintf("%s:%d", cfg.ServerAddress, cfg.ServerPort))
		if server == nil {
			ErrorLogger.Panic("Unable to start server:", err)
		}
		clientChannel := clientConnections(server, cfg)
		for {
			go handleConnection(<-clientChannel, cfg)
		}
	}
}
