/*
quotaflag
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"fmt"
	"github.com/akamensky/argparse"
	"github.com/go-ldap/ldap/v3"
	"net"
	"os"
	"reflect"
	"strconv"
	"strings"
)

// Defaults
const (
	serverHttpAddress = "http://127.0.0.1:10080/recipient_address"
	serverAddress     = "127.0.0.1"
	serverPort        = 4648
	maxRetries        = 9
)

const (
	logLevelNone  = iota
	logLevelInfo  = iota
	logLevelDebug = iota
)

const (
	BASE = "base"
	ONE  = "one"
	SUB  = "sub"
)

type CmdLineConfig struct {
	// Listen address for the policy service
	ServerAddress string

	// Port number for the policy service
	ServerPort int

	// DEFER and REJECT messages
	DeferText  string
	RejectText string

	// SASL username request endpoint for the over quota flag value
	UseHTTP     bool
	HttpAddress string
	UseLDAP     bool
	LDAP

	VerboseLevel int

	// Flag that indicates which command was called
	CommandServer bool
}

func (c *CmdLineConfig) String() string {
	var result string

	v := reflect.ValueOf(*c)
	typeOfc := v.Type()

	for i := 0; i < v.NumField(); i++ {
		switch typeOfc.Field(i).Name {
		case "CommandServer", "Database", "LDAP", "VerboseLevel":
			continue
		default:
			result += fmt.Sprintf(" %s='%v'", typeOfc.Field(i).Name, v.Field(i).Interface())
		}
	}

	return result[1:]
}

func (c *CmdLineConfig) Init(args []string) {
	parser := argparse.NewParser("quotaflag", "Recipient user mailbox quota flag")

	commandServer := parser.NewCommand("server", "Run a quotaflag policy server")

	/*
	 * Quota flag server options
	 */
	argServerAddress := commandServer.String(
		"a", "server-address", &argparse.Options{
			Required: false,
			Default:  serverAddress,
			Validate: func(opt []string) error {
				if addr := net.ParseIP(opt[0]); addr == nil {
					if _, err := net.LookupHost(opt[0]); err != nil {
						return fmt.Errorf("%s is not a valid IP address or hostname", opt[0])
					}
				}
				return nil
			},
			Help: "IPv4 or IPv6 address for the policy service",
		})
	argServerPort := commandServer.Int(
		"p", "server-port", &argparse.Options{
			Required: false,
			Default:  serverPort,
			Validate: func(opt []string) error {
				if arg, err := strconv.Atoi(opt[0]); err != nil {
					return fmt.Errorf("%s is not an integer", opt[0])
				} else {
					if !(arg > 0 && arg <= 65535) {
						return fmt.Errorf("%s is not a valid port number", opt[0])
					}
				}
				return nil
			},
			Help: "Port for the policy service",
		})
	argServerUseHTTP := commandServer.Flag(
		"", "use-http", &argparse.Options{
			Required: false,
			Default:  false,
			Help:     "Enable HTTP support",
		})
	argServerHttpAddress := commandServer.String(
		"", "http-address", &argparse.Options{
			Required: false,
			Default:  serverHttpAddress,
			Help:     "HTTP address for the HTTP username endpoint",
		})
	argServerDeferText := commandServer.String(
		"", "defer-text", &argparse.Options{
			Required: false,
			Default:  deferText,
			Help:     "Postfix access(5) reply message that is sent if the service is temporarily broken",
		})
	argServerRejectText := commandServer.String(
		"", "reject-text", &argparse.Options{
			Required: false,
			Default:  rejectText,
			Help:     "Postfix access(5) reply message that is sent if the recipient mailbox is over quota",
		})

	/*
	 * LDAP settings
	 */
	argServerUseLDAP := commandServer.Flag(
		"", "use-ldap", &argparse.Options{
			Required: false,
			Default:  false,
			Help:     "Enable LDAP support",
		})
	argServerLDAPServerURIs := commandServer.StringList(
		"", "ldap-server-uri", &argparse.Options{
			Required: false,
			Default:  []string{"ldap://127.0.0.1:389/"},
			Help:     "Server URI. Specify multiple times, if you need more than one server",
		})
	argServerLDAPBaseDN := commandServer.String(
		"", "ldap-basedn", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "Base DN",
		})
	argServerLDAPBindDN := commandServer.String(
		"", "ldap-binddn", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "bind DN",
		})
	argServerLDAPBindPWPATH := commandServer.String(
		"", "ldap-bindpw", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "bind password",
		})
	argServerLDAPFilter := commandServer.String(
		"", "ldap-filter", &argparse.Options{
			Required: false,
			Default:  "(&(objectClass=*)(mailAlias=%s))",
			Help:     "Filter with %s placeholder",
		})
	argServerLDAPResultAttr := commandServer.String(
		"", "ldap-result-attribute", &argparse.Options{
			Required: false,
			Default:  "quotaflag",
			Help:     "Result attribute for the requested mail recipient",
		})
	argServerLDAPStartTLS := commandServer.Flag(
		"", "ldap-starttls", &argparse.Options{
			Required: false,
			Default:  false,
			Help:     "If this option is given, use StartTLS",
		})
	argServerLDAPTLSVerify := commandServer.Flag(
		"", "ldap-tls-skip-verify", &argparse.Options{
			Required: false,
			Default:  false,
			Help:     "Skip TLS server name verification",
		})
	argServerLDAPTLSCAFile := commandServer.String(
		"", "ldap-tls-cafile", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "File containing TLS CA certificate(s)",
		})
	argServerLDAPTLSClientCert := commandServer.String(
		"", "ldap-tls-client-cert", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "File containing a TLS client certificate",
		})
	argServerLDAPTLSClientKey := commandServer.String(
		"", "ldap-tls-client-key", &argparse.Options{
			Required: false,
			Default:  "",
			Help:     "File containing a TLS client key",
		})
	argServerLDAPSASLExternal := commandServer.Flag(
		"", "ldap-sasl-external", &argparse.Options{
			Required: false,
			Default:  false,
			Help:     "Use SASL/EXTERNAL instead of a simple bind",
		})
	argServerLDAPScope := commandServer.String(
		"", "ldap-scope", &argparse.Options{
			Required: false,
			Default:  "sub",
			Validate: func(opt []string) error {
				switch opt[0] {
				case BASE, ONE, SUB:
					return nil
				default:
					return fmt.Errorf("value '%s' must be one of: 'one', 'base' or 'sub'", opt[0])
				}
			},
			Help: "LDAP search scope [base, one, sub]",
		})

	/*
	 * Other settings
	 */
	argVerbose := parser.FlagCounter(
		"v", "verbose", &argparse.Options{
			Help: "Verbose mode. Repeat this for an increased log level",
		})
	argVersion := parser.Flag(
		"", "version", &argparse.Options{
			Help: "Current version",
		})

	err := parser.Parse(args)
	if err != nil {
		ErrorLogger.Fatalln(parser.Usage(err))
	}

	if *argVersion {
		fmt.Println("Version:", version)
		os.Exit(0)
	}

	if val := os.Getenv("VERBOSE"); val != "" {
		switch val {
		case "none":
			c.VerboseLevel = logLevelNone
		case "info":
			c.VerboseLevel = logLevelInfo
		case "debug":
			c.VerboseLevel = logLevelDebug
		}
	} else {
		switch *argVerbose {
		case logLevelNone:
			c.VerboseLevel = logLevelNone
		case logLevelInfo:
			c.VerboseLevel = logLevelInfo
		case logLevelDebug:
			c.VerboseLevel = logLevelDebug
		default:
			c.VerboseLevel = logLevelDebug
		}
	}

	c.CommandServer = commandServer.Happened()

	if commandServer.Happened() {
		if val := os.Getenv("SERVER_ADDRESS"); val != "" {
			c.ServerAddress = val
		} else {
			c.ServerAddress = *argServerAddress
		}
		if val := os.Getenv("SERVER_PORT"); val != "" {
			p, err := strconv.Atoi(val)
			if err != nil {
				ErrorLogger.Fatalln("Error: SERVER_PORT an not be used:", parser.Usage(err))
			}
			c.ServerPort = p
		} else {
			c.ServerPort = *argServerPort
		}
		if val := os.Getenv("DEFER_TEXT"); val != "" {
			c.DeferText = val
		} else {
			c.DeferText = *argServerDeferText
		}
		if val := os.Getenv("REJECT_TEXT"); val != "" {
			c.RejectText = val
		} else {
			c.RejectText = *argServerRejectText
		}
		if val := os.Getenv("USE_HTTP"); val != "" {
			p, err := strconv.ParseBool(val)
			if err != nil {
				ErrorLogger.Fatalln("Error:", err)
			}
			c.UseHTTP = p
		} else {
			c.UseHTTP = *argServerUseHTTP
		}
		if val := os.Getenv("HTTP_ADDRESS"); val != "" {
			c.HttpAddress = val
		} else {
			c.HttpAddress = *argServerHttpAddress
		}

		if val := os.Getenv("USE_LDAP"); val != "" {
			p, err := strconv.ParseBool(val)
			if err != nil {
				ErrorLogger.Fatalln("Error:", err)
			}
			c.UseLDAP = p
		} else {
			c.UseLDAP = *argServerUseLDAP
		}
		if c.UseLDAP {
			if val := os.Getenv("LDAP_SERVER_URIS"); val != "" {
				p := strings.Split(val, ",")
				for i, uri := range p {
					p[i] = strings.TrimSpace(uri)
				}
				c.LDAP.ServerURIs = p
			} else {
				c.LDAP.ServerURIs = *argServerLDAPServerURIs
			}
			if val := os.Getenv("LDAP_BASEDN"); val != "" {
				c.LDAP.BaseDN = val
			} else {
				c.LDAP.BaseDN = *argServerLDAPBaseDN
			}
			if val := os.Getenv("LDAP_BINDDN"); val != "" {
				c.LDAP.BindDN = val
			} else {
				c.LDAP.BindDN = *argServerLDAPBindDN
			}
			if val := os.Getenv("LDAP_BINDPW"); val != "" {
				c.LDAP.BindPW = val
			} else {
				c.LDAP.BindPW = *argServerLDAPBindPWPATH
			}
			if val := os.Getenv("LDAP_FILTER"); val != "" {
				c.LDAP.Filter = val
			} else {
				c.LDAP.Filter = *argServerLDAPFilter
			}
			if val := os.Getenv("LDAP_RESULT_ATTRIBUTE"); val != "" {
				c.LDAP.ResultAttr = []string{val}
			} else {
				c.LDAP.ResultAttr = []string{*argServerLDAPResultAttr}
			}
			if val := os.Getenv("LDAP_STARTTLS"); val != "" {
				p, err := strconv.ParseBool(val)
				if err != nil {
					ErrorLogger.Fatalln("Error:", err)
				}
				c.LDAP.StartTLS = p
			} else {
				c.LDAP.StartTLS = *argServerLDAPStartTLS
			}
			if val := os.Getenv("LDAP_TLS_SKIP_VERIFY"); val != "" {
				p, err := strconv.ParseBool(val)
				if err != nil {
					ErrorLogger.Fatalln("Error:", err)
				}
				c.LDAP.TLSSkipVerify = p
			} else {
				c.LDAP.TLSSkipVerify = *argServerLDAPTLSVerify
			}
			if val := os.Getenv("LDAP_TLS_CAFILE"); val != "" {
				c.LDAP.TLSCAFile = val
			} else {
				c.LDAP.TLSCAFile = *argServerLDAPTLSCAFile
			}
			if val := os.Getenv("LDAP_TLS_CLIENT_CERT"); val != "" {
				c.LDAP.TLSClientCert = val
			} else {
				c.LDAP.TLSClientCert = *argServerLDAPTLSClientCert
			}
			if val := os.Getenv("LDAP_TLS_CLIENT_KEY"); val != "" {
				c.LDAP.TLSClientKey = val
			} else {
				c.LDAP.TLSClientKey = *argServerLDAPTLSClientKey
			}
			if val := os.Getenv("LDAP_SASL_EXTERNAL"); val != "" {
				p, err := strconv.ParseBool(val)
				if err != nil {
					ErrorLogger.Fatalln("Error:", err)
				}
				c.LDAP.SASLExternal = p
			} else {
				c.LDAP.SASLExternal = *argServerLDAPSASLExternal
			}
			if val := os.Getenv("LDAP_SCOPE"); val != "" {
				switch val {
				case BASE:
					c.LDAP.Scope = ldap.ScopeBaseObject
				case ONE:
					c.LDAP.Scope = ldap.ScopeSingleLevel
				case SUB:
					c.LDAP.Scope = ldap.ScopeWholeSubtree
				default:
					ErrorLogger.Fatalln(parser.Usage(fmt.Sprintf("value '%s' must be one of: one, base or sub", val)))
				}
			} else {
				switch *argServerLDAPScope {
				case BASE:
					c.LDAP.Scope = ldap.ScopeBaseObject
				case ONE:
					c.LDAP.Scope = ldap.ScopeSingleLevel
				case SUB:
					c.LDAP.Scope = ldap.ScopeWholeSubtree
				}
			}
		}

		// Logical XOR
		if !(c.UseHTTP == false != c.UseLDAP == false) {
			ErrorLogger.Println("You must either use HTTP or LDAP")
			ErrorLogger.Fatalln(parser.Usage(err))
		}
	}
}
