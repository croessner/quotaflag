/*
quotaflag
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"bufio"
	"fmt"
	"github.com/segmentio/ksuid"
	"net"
	"strings"
)

func clientConnections(listener net.Listener, cfg *CmdLineConfig) chan net.Conn {
	ch := make(chan net.Conn)
	go func() {
		for {
			client, err := listener.Accept()
			if client == nil {
				ErrorLogger.Println("Couldn't accept connection:", err)
				continue
			}
			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("Client %v connected\n", client.RemoteAddr())
			}
			ch <- client
		}
	}()
	return ch
}

//goland:noinspection GoUnhandledErrorResult
func handleConnection(client net.Conn, cfg *CmdLineConfig) {
	b := bufio.NewReader(client)
	var policyRequest = make(map[string]string)

	for {
		lineBytes, err := b.ReadBytes('\n')
		if err != nil { // EOF, or worse
			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("Client %v disconnected\n", client.RemoteAddr())
			}
			client.Close()
			break
		}

		lineStr := strings.TrimSpace(string(lineBytes))
		items := strings.SplitN(lineStr, "=", 2)
		if len(items) == 2 {
			policyRequest[strings.TrimSpace(items[0])] = strings.TrimSpace(items[1])
		} else {
			var (
				recipient     string
				clientAddress string
			)

			failed := false
			actionText := "DUNNO"

			if val, ok := policyRequest["recipient"]; ok {
				recipient = val
			}
			if val, ok := policyRequest["client_address"]; ok {
				clientAddress = val
			}

			if recipient == "" {
				recipient = "-"
			}
			if clientAddress == "" {
				clientAddress = "-"
			}

			p := &Policy{policyRequest: policyRequest}
			p.guid = ksuid.New().String()

			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("guid=\"%s\" %+v\n", p.guid, policyRequest)
			}
			err := p.IsOverQuota(cfg)
			if err != nil {
				failed = true
				ErrorLogger.Println(err)
				actionText = cfg.DeferText
			} else {
				if p.quotaFlag {
					actionText = cfg.RejectText
				}
			}

			if cfg.VerboseLevel >= logLevelInfo {
				InfoLogger.Printf("guid=\"%s\" client_address=\"%s\" recipient=\"%s\" quota_flag=\"%v\" errors=\"%v\" action=\"%s\"\n",
					p.guid, clientAddress, recipient, p.quotaFlag, failed, actionText)
			}

			client.Write([]byte(fmt.Sprintf("action=%s\n\n", actionText)))
			policyRequest = make(map[string]string) // Clear policy request for next connection
		}
	}
}
