package main

import (
	"fmt"
	"github.com/go-ldap/ldap/v3"
	"os"
	"testing"
)

func envSetter(envs map[string]string) (closer func()) {
	originalEnvs := map[string]string{}

	for name, value := range envs {
		if originalValue, ok := os.LookupEnv(name); ok {
			originalEnvs[name] = originalValue
		}
		_ = os.Setenv(name, value)
	}

	return func() {
		for name := range envs {
			origValue, has := originalEnvs[name]
			if has {
				_ = os.Setenv(name, origValue)
			} else {
				_ = os.Unsetenv(name)
			}
		}
	}
}

func TestConfigVerboseNone(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http"})
	if cfg.VerboseLevel != logLevelNone {
		t.Errorf("Expected --verbose not set, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigEnvVerboseNone(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP": "true",
		"VERBOSE":  "none",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.VerboseLevel != logLevelNone {
		t.Errorf("Expected --verbose not set, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigVerboseInfo(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--verbose"})
	if cfg.VerboseLevel != logLevelInfo {
		t.Errorf("Expected --verbose, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigEnvVerboseInfo(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP": "true",
		"VERBOSE":  "info",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.VerboseLevel != logLevelInfo {
		t.Errorf("Expected --verbose, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigVerboseDebug(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--verbose", "--verbose"})
	if cfg.VerboseLevel != logLevelDebug {
		t.Errorf("Expected --verbose --verbose, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigEnvVerboseDebug(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP": "true",
		"VERBOSE":  "debug",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.VerboseLevel != logLevelDebug {
		t.Errorf("Expected --verbose --verbose, got value=%v", cfg.VerboseLevel)
	}
}

func TestConfigServerAddress(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--server-address", "172.16.23.45"})
	if cfg.ServerAddress != "172.16.23.45" {
		t.Errorf("Expected --server-address=172.16.23.45, got value=%v", cfg.ServerAddress)
	}
}

func TestConfigEnvServerAddress(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP":       "true",
		"SERVER_ADDRESS": "172.16.23.45",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.ServerAddress != "172.16.23.45" {
		t.Errorf("Expected --server-address=172.16.23.45, got value=%v", cfg.ServerAddress)
	}
}

func TestConfigServerPort(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--server-port", "9000"})
	if cfg.ServerPort != 9000 {
		t.Errorf("Expected --server-port=9000, got value=%v", cfg.ServerPort)
	}
}

func TestConfigEnvServerPort(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP":    "true",
		"SERVER_PORT": "9000",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.ServerPort != 9000 {
		t.Errorf("Expected --server-port=9000, got value=%v", cfg.ServerPort)
	}
}

func TestConfigHttpAddress(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--http-address", "http://127.0.0.1:44888/"})
	if cfg.HttpAddress != "http://127.0.0.1:44888/" {
		t.Errorf("Expected --http-address=http://127.0.0.1:44888/, got value=%v", cfg.HttpAddress)
	}
}

func TestConfigEnvHttpAddress(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP":     "true",
		"HTTP_ADDRESS": "http://127.0.0.1:44888/",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.HttpAddress != "http://127.0.0.1:44888/" {
		t.Errorf("Expected --http-address=http://127.0.0.1:44888/, got value=%v", cfg.HttpAddress)
	}
}

func TestConfigDeferText(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--defer-text", "DEFER test text"})
	if cfg.DeferText != "DEFER test text" {
		t.Errorf("Expected --defer-text='DEFER test text', got value=%v", cfg.DeferText)
	}
}

func TestConfigEnvDeferText(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP":   "true",
		"DEFER_TEXT": "DEFER test text",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.DeferText != "DEFER test text" {
		t.Errorf("Expected --defer-text='DEFER test text', got value=%v", cfg.DeferText)
	}
}

func TestConfigRejectText(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-http", "--reject-text", "REJECT test text"})
	if cfg.RejectText != "REJECT test text" {
		t.Errorf("Expected --reject-text='REJECT test text', got value=%v", cfg.RejectText)
	}
}

func TestConfigEnvRejectText(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_HTTP":    "true",
		"REJECT_TEXT": "REJECT test text",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.RejectText != "REJECT test text" {
		t.Errorf("Expected --reject-text='REJECT test text', got value=%v", cfg.RejectText)
	}
}

func TestConfigUseLDAP(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap"})
	if cfg.UseLDAP != true {
		t.Errorf("Expected --use-ldap, got value=%v", cfg.UseLDAP)
	}
}

func TestConfigEnvUseLDAP(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP": "true",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.UseLDAP != true {
		t.Errorf("Expected --use-ldap, got value=%v", cfg.UseLDAP)
	}
}

func TestConfigLDAPServerUris(t *testing.T) {
	cfg := new(CmdLineConfig)
	u1 := "ldap://localhost:389/"
	u2 := "ldap://example.com:389/"
	f1 := false
	f2 := false
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-server-uri", u1, "--ldap-server-uri", u2})
	for _, v := range cfg.LDAP.ServerURIs {
		if v == u1 {
			f1 = true
			break
		}
	}
	for _, v := range cfg.LDAP.ServerURIs {
		if v == u2 {
			f2 = true
			break
		}
	}
	if f1 != true && f2 != true {
		t.Errorf("Expected --ldap-server-uri=%s --ldap-server-uri=%s, got value=%v", u1, u2, cfg.LDAP.ServerURIs)
	}
}

func TestConfigEnvLDAPServerUris(t *testing.T) {
	u1 := "ldap://localhost:389/"
	u2 := "ldap://example.com:389/"
	closer := envSetter(map[string]string{
		"USE_LDAP":         "true",
		"LDAP_SERVER_URIS": fmt.Sprintf("%s, %s", u1, u2),
	})
	defer closer()
	cfg := new(CmdLineConfig)
	f1 := false
	f2 := false
	cfg.Init([]string{"app", "server"})
	for _, v := range cfg.LDAP.ServerURIs {
		if v == u1 {
			f1 = true
			break
		}
	}
	for _, v := range cfg.LDAP.ServerURIs {
		if v == u2 {
			f2 = true
			break
		}
	}
	if f1 != true && f2 != true {
		t.Errorf("Expected --ldap-server-uri=%s --ldap-server-uri=%s, got value=%v", u1, u2, cfg.LDAP.ServerURIs)
	}
}

func TestConfigLDAPBaseDN(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-basedn", "o=org"})
	if cfg.LDAP.BaseDN != "o=org" {
		t.Errorf("Expected --ldap-basedn=o=org, got value=%v", cfg.LDAP.BaseDN)
	}
}

func TestConfigEnvLDAPBaseDN(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":    "true",
		"LDAP_BASEDN": "o=org",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.BaseDN != "o=org" {
		t.Errorf("Expected --ldap-basedn=o=org, got value=%v", cfg.LDAP.BaseDN)
	}
}

func TestConfigLDAPBindDN(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-binddn", "cn=admin,o=org"})
	if cfg.LDAP.BindDN != "cn=admin,o=org" {
		t.Errorf("Expected --ldap-binddn=cn=admin,o=org, got value=%v", cfg.LDAP.BindDN)
	}
}

func TestConfigEnvLDAPBindDN(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":    "true",
		"LDAP_BINDDN": "cn=admin,o=org",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.BindDN != "cn=admin,o=org" {
		t.Errorf("Expected --ldap-binddn=cn=admin,o=org, got value=%v", cfg.LDAP.BindDN)
	}
}

func TestConfigLDAPBindPW(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-bindpw", "password"})
	if cfg.LDAP.BindPW != "password" {
		t.Errorf("Expected --ldap-bindpw=password, got value=%v", cfg.LDAP.BindPW)
	}
}

func TestConfigEnvLDAPBindPW(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":    "true",
		"LDAP_BINDPW": "password",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.BindPW != "password" {
		t.Errorf("Expected --ldap-bindpw=password, got value=%v", cfg.LDAP.BindPW)
	}
}

func TestConfigLDAPFilter(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-filter", "(objectClass=*)"})
	if cfg.LDAP.Filter != "(objectClass=*)" {
		t.Errorf("Expected --ldap-filter=(objectClass=*), got value=%v", cfg.LDAP.Filter)
	}
}

func TestConfigEnvLDAPFilter(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":    "true",
		"LDAP_FILTER": "(objectClass=*)",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.Filter != "(objectClass=*)" {
		t.Errorf("Expected --ldap-filter=(objectClass=*), got value=%v", cfg.LDAP.Filter)
	}
}

func TestConfigLDAPResultAttribute(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-result-attribute", "mail"})
	if cfg.LDAP.ResultAttr[0] != "mail" {
		t.Errorf("Expected --ldap-result-attribute=mail, got value=%v", cfg.LDAP.ResultAttr)
	}
}

func TestConfigEnvLDAPResultAttribute(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":              "true",
		"LDAP_RESULT_ATTRIBUTE": "mail",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.ResultAttr[0] != "mail" {
		t.Errorf("Expected --ldap-result-attribute=mail, got value=%v", cfg.LDAP.ResultAttr)
	}
}

func TestConfigLDAPStartTLS(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-starttls"})
	if cfg.LDAP.StartTLS != true {
		t.Errorf("Expected --ldap-starttls, got value=%v", cfg.LDAP.StartTLS)
	}
}

func TestConfigEnvLDAPStartTLS(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":      "true",
		"LDAP_STARTTLS": "true",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.StartTLS != true {
		t.Errorf("Expected --ldap-starttls, got value=%v", cfg.LDAP.StartTLS)
	}
}

func TestConfigLDAPTLSSkipVerify(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-tls-skip-verify"})
	if cfg.LDAP.TLSSkipVerify != true {
		t.Errorf("Expected --ldap-tls-skip-verify, got value=%v", cfg.LDAP.TLSSkipVerify)
	}
}

func TestConfigEnvLDAPTLSSkipVerify(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":             "true",
		"LDAP_TLS_SKIP_VERIFY": "true",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.TLSSkipVerify != true {
		t.Errorf("Expected --ldap-tls-skip-verify, got value=%v", cfg.LDAP.TLSSkipVerify)
	}
}

func TestConfigLDAPTLSClientCert(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-tls-client-cert", "/tmp"})
	if cfg.LDAP.TLSClientCert != "/tmp" {
		t.Errorf("Expected --ldap-tls-client-cert=/tmp, got value=%v", cfg.LDAP.TLSClientCert)
	}
}

func TestConfigEnvLDAPTLSClientCert(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":             "true",
		"LDAP_TLS_CLIENT_CERT": "/tmp",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.TLSClientCert != "/tmp" {
		t.Errorf("Expected --ldap-tls-client-cert=/tmp, got value=%v", cfg.LDAP.TLSClientCert)
	}
}

func TestConfigLDAPTLSClientKey(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-tls-client-key", "/tmp"})
	if cfg.LDAP.TLSClientKey != "/tmp" {
		t.Errorf("Expected --ldap-tls-client-key=/tmp, got value=%v", cfg.LDAP.TLSClientKey)
	}
}

func TestConfigEnvLDAPTLSClientKey(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":            "true",
		"LDAP_TLS_CLIENT_KEY": "/tmp",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.TLSClientKey != "/tmp" {
		t.Errorf("Expected --ldap-tls-client-key=/tmp, got value=%v", cfg.LDAP.TLSClientKey)
	}
}

func TestConfigLDAPSASLExternal(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-sasl-external"})
	if cfg.LDAP.SASLExternal != true {
		t.Errorf("Expected --ldap-sasl-external, got value=%v", cfg.LDAP.SASLExternal)
	}
}

func TestConfigEnvLDAPSASLExternal(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":           "true",
		"LDAP_SASL_EXTERNAL": "true",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.SASLExternal != true {
		t.Errorf("Expected --ldap-sasl-external, got value=%v", cfg.LDAP.SASLExternal)
	}
}

func TestConfigLDAPScopeBase(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-scope", BASE})
	if cfg.LDAP.Scope != ldap.ScopeBaseObject {
		t.Errorf("Expected --ldap-scope=base (%d), got value=%v", ldap.ScopeBaseObject, cfg.LDAP.Scope)
	}
}

func TestConfigEnvLDAPScopeBase(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":   "true",
		"LDAP_SCOPE": "base",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.Scope != ldap.ScopeBaseObject {
		t.Errorf("Expected --ldap-scope=base (%d), got value=%v", ldap.ScopeBaseObject, cfg.LDAP.Scope)
	}
}

func TestConfigLDAPScopeOne(t *testing.T) {
	cfg := CmdLineConfig{}
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-scope", ONE})
	if cfg.LDAP.Scope != ldap.ScopeSingleLevel {
		t.Errorf("Expected --ldap-scope=one (%d), got value=%v", ldap.ScopeSingleLevel, cfg.LDAP.Scope)
	}
}

func TestConfigEnvLDAPScopeOne(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":   "true",
		"LDAP_SCOPE": "one",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.Scope != ldap.ScopeSingleLevel {
		t.Errorf("Expected --ldap-scope=one (%d), got value=%v", ldap.ScopeSingleLevel, cfg.LDAP.Scope)
	}
}

func TestConfigLDAPScopeSub(t *testing.T) {
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server", "--use-ldap", "--ldap-scope", SUB})
	if cfg.LDAP.Scope != ldap.ScopeWholeSubtree {
		t.Errorf("Expected --ldap-scope=sub (%d), got value=%v", ldap.ScopeWholeSubtree, cfg.LDAP.Scope)
	}
}

func TestConfigEnvLDAPScopeSub(t *testing.T) {
	closer := envSetter(map[string]string{
		"USE_LDAP":   "true",
		"LDAP_SCOPE": "sub",
	})
	defer closer()
	cfg := new(CmdLineConfig)
	cfg.Init([]string{"app", "server"})
	if cfg.LDAP.Scope != ldap.ScopeWholeSubtree {
		t.Errorf("Expected --ldap-scope=sub (%d), got value=%v", ldap.ScopeWholeSubtree, cfg.LDAP.Scope)
	}
}
