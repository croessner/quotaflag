# quotaflag

## About

Postfix policy server that checks a mailbox quota flag using a HTTP post request or a LDAP server.

## Features

* Receive quota flag from HTTP server
* Receive quota flag from LDAP
* Reject a sender, if the recipient mailbox is over quota

# Table of contents

1. [Install](#install)
   * [Postfix integration](#postfix-integration)
   * [Preparing a docker image](#preparing-a-docker-image)
   * [Build from source](#build-from-source)
   * [Server options](#server-options)
2. [Environment variables](#environment-variables)
   * [Server](#server)
3. [HTTP request](#http-request)

# Install

## Postfix integration

The service is configured in Postfix like this...

```
smtpd_recipient_restrictions =
    ...
    check_policy_service inet:127.0.0.1:4648
    ...
```

... if you use the docker-compose.yml file as provided.

Back to [table of contents](#table-of-contents)

## Preparing a docker image

The simplest way to use the program is by using a docker image.

```shell
cd /path/to/Dockerfile
docker build -t quotaflag:latest .
```

Back to [table of contents](#table-of-contents)

## Build from source

### Requirements

To build quotaflag from source, you need at least Golang version 1.17 or above.

### Compile

Build the main application:

```shell
cd /path/to/sourcecode
go mod tidy
go build -o quotaflag .
```

You can find an example systemd unit file as well as a default configuration file in the contrib folder.

Back to [table of contents](#table-of-contents)

## Server options

```shell
ratelimit server --help
```

produces the following output:

```
...

  -h  --help                   Print help information
  -a  --server-address         IPv4 or IPv6 address for the policy service. Default: 127.0.0.1
  -p  --server-port            Port for the policy service. Default: 4648
      --use-http               Enable HTTP support. Default: false
      --http-address           HTTP address for the HTTP username endpoint. Default: http://127.0.0.1:10080/recipient_address
      --defer-text             Postfix access(5) reply message that is sent if the service is temporarily broken. Default: DEFER Service temporarily not available
      --reject-text            Postfix access(5) reply message that is sent if the recipient mailbox is over quota. Default: REJECT User mailbox is over quota
      --use-ldap               Enable LDAP support. Default: false
      --ldap-server-uri        Server URI. Specify multiple times, if you need more than one server. Default: [ldap://127.0.0.1:389/]
      --ldap-basedn            Base DN. Default: 
      --ldap-binddn            bind DN. Default: 
      --ldap-bindpw            bind password. Default: 
      --ldap-filter            Filter with %s placeholder. Default: (&(objectClass=*)(mailAlias=%s))
      --ldap-result-attribute  Result attribute for the requested mail sender. Default: quotaflag
      --ldap-starttls          If this option is given, use StartTLS. Default: false
      --ldap-tls-skip-verify   Skip TLS server name verification. Default: false
      --ldap-tls-cafile        File containing TLS CA certificate(s). Default:  
      --ldap-tls-client-cert   File containing a TLS client certificate. Default: 
      --ldap-tls-client-key    File containing a TLS client key. Default: 
      --ldap-sasl-external     Use SASL/EXTERNAL instead of a simple bind. Default: false
      --ldap-scope             LDAP search scope [base, one, sub]. Default: sub
  -v  --verbose                Verbose mode. Repeat this for an increased log level
      --version                Current version
```

Back to [table of contents](#table-of-contents)

# Environment variables

The following environment variables can be used to configure the policy service. This is especially useful, if you plan
on running the service as a docker service.

## Server

Variable | Description
---|---
SERVER_ADDRESS | IPv4 or IPv6 address for the policy service; default(127.0.0.1)
SERVER_PORT | Port for the policy service; default(4648)
USE_HTTP | Enable HTTP support
HTTP_ADDRESS | HTTP address for the HTTP username endpoint; default(http://127.0.0.1:10080/recipient_address)
DEFER_TEXT | Postfix access(5) reply message that is sent if the service is temporarily broken; default(DEFER Service temporarily not available)
REJECT_TEXT | Postfix access(5) reply message that is sent if the sender rate was over limit; default(REJECT User mailbox is over quota)
USE_LDAP | Enable LDAP support; default(false)
LDAP_SERVER_URIS | Server URI. Specify multiple times, if you need more than one server; default(ldap://127.0.0.1:389/)
LDAP_BASEDN | Base DN
LDAP_BINDPW | Bind PW
LDAP_FILTER | Filter with %s placeholder; default( (&(objectClass=*)(mailAlias=%s)) )
LDAP_RESULT_ATTRIBUTE | Result attribute for the requested mail sender; default(mailAccount)
LDAP_STARTTLS | If this option is given, use StartTLS
LDAP_TLS_SKIP_VERIFY | Skip TLS server name verification
LDAP_TLS_CAFILE | File containing TLS CA certificate(s)
LDAP_TLS_CLIENT_CERT | File containing a TLS client certificate
LDAP_TLS_CLIENT_KEY | File containing a TLS client key
LDAP_SASL_EXTERNAL | Use SASL/EXTERNAL instead of a simple bind; default(false)
LDAP_SCOPE | LDAP search scope [base, one, sub]; default(sub)
VERBOSE | Log level. One of 'none', 'info' or 'debug'

Back to [table of contents](#table-of-contents)

## HTTP request

The policy service sends a policy request like the following example demonstrates:

```shell
curl -i -X POST http://127.0.0.1:10080/recipient_address -H 'Content-Type: application/json' -d '{"policy":[{"type":"quotaflag", "email":"test@example.test"}]}'
```

The result mus be exactly like this:

```json
[{"type":"quotaflag","email":"test@example.test","quotaflag":BOOL}]
```

```
version: "3.8"

services:
      
  quotaflag:
    image: quotaflag:latest
    logging:
      driver: journald
      options:
        tag: quotaflag
    restart: always
    environment:
      VERBOSE: "debug"
      SERVER_ADDRESS: "0.0.0.0"
      SERVER_PORT: "4648"
      HTTP_ADDRESS: http://samplehttp:10080/recipient_address
    ports:
      - "127.0.0.1:4648:4648"
```

Back to [table of contents](#table-of-contents)

Hope you enjoy :-)