/*
quotaflag
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const httpRequestTimeout = time.Second * 300

type RequestData struct {
	Type  string `json:"type"`
	Email string `json:"email"`
}
type HttpRequest struct {
	Policy []RequestData `json:"policy"`
}

type HttpResponse struct {
	Type      string `json:"type"`
	Email     string `json:"email"`
	QuotaFlag bool   `json:"quotaflag"`
}

func HasContentType(response *http.Response, mimetype string) bool {
	contentType := response.Header.Get("Content-type")
	for _, v := range strings.Split(contentType, ",") {
		t, _, err := mime.ParseMediaType(v)
		if err != nil {
			break
		}
		if t == mimetype {
			return true
		}
	}
	return false
}

func getQuotaFlagHttp(recipient string, httpuri string, verboseLevel int, instance string) (bool, error) {
	var err error
	var requestData []RequestData
	var httpClient = &http.Client{
		Timeout: httpRequestTimeout,
	}
	response := new([]HttpResponse)

	requestData = append(requestData, RequestData{Type: "quotaflag", Email: recipient})
	values := &HttpRequest{Policy: requestData}
	request, err := json.Marshal(values)
	if verboseLevel == logLevelDebug {
		DebugLogger.Printf("guid=\"%s\" HTTP-request: %s\n", instance, string(request))
	}
	if err != nil {
		return false, err
	}
	httpResponse, err := httpClient.Post(httpuri, "application/json", bytes.NewBuffer(request))
	if err != nil {
		return false, err
	}
	if !HasContentType(httpResponse, "application/json") {
		return false, fmt.Errorf("wrong Content-Type: %s", httpResponse.Header.Get("Content-type"))
	}
	responseData, err := ioutil.ReadAll(httpResponse.Body)
	if verboseLevel == logLevelDebug {
		DebugLogger.Printf("guid=\"%s\" HTTP-response: %s\n", instance, strings.TrimSpace(string(responseData)))
	}
	if err != nil {
		return false, err
	}
	err = json.Unmarshal(responseData, response)
	if err != nil {
		return false, err
	}
	if len(*response) != 1 {
		return false, fmt.Errorf("expecting exactly one result, got: %v", response)
	}

	return (*response)[0].QuotaFlag, nil
}

func getQuotaFlagLdap(recipient string, verboseLevel int, instance string) (bool, error) {
	var err error
	var ldapResult string

	if ldapResult, err = ldapServer.search(recipient, verboseLevel, instance); err != nil {
		InfoLogger.Println(err)
		if !strings.Contains(fmt.Sprint(err), "No Such Object") {
			if ldapServer.LDAPConn != nil {
				ldapServer.LDAPConn.Close()
			}
			if err := ldapServer.connect(verboseLevel, instance); err != nil {
				return false, err
			}
			ldapServer.bind(verboseLevel, instance)
			ldapResult, err = ldapServer.search(recipient, verboseLevel, instance)
			if err != nil {
				return false, err
			}
		}
	}

	if ldapResult != "" {
		p, err := strconv.ParseBool(ldapResult)
		if err != nil {
			return false, err
		}
		return p, nil
	}

	return false, fmt.Errorf("no result from LDAP server(s)")
}
