/*
quotaflag
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/go-ldap/ldap/v3"
	"io/ioutil"
	"net"
	"net/url"
	"reflect"
	"strings"
	"sync"
)

type LDAP struct {
	ServerURIs    []string
	BaseDN        string
	BindDN        string
	BindPW        string
	Filter        string
	ResultAttr    []string
	StartTLS      bool
	TLSSkipVerify bool
	TLSCAFile     string
	TLSClientCert string
	TLSClientKey  string
	SASLExternal  bool
	Scope         int

	Mu       *sync.Mutex
	LDAPConn *ldap.Conn
}

func (l *LDAP) String() string {
	var result string

	v := reflect.ValueOf(*l)
	typeOfc := v.Type()

	for i := 0; i < v.NumField(); i++ {
		switch typeOfc.Field(i).Name {
		case "Mu", "LDAPConn":
			continue
		case "Scope":
			switch l.Scope {
			case ldap.ScopeBaseObject:
				result += fmt.Sprintf(" %s='base'", typeOfc.Field(i).Name)
			case ldap.ScopeSingleLevel:
				result += fmt.Sprintf(" %s='one'", typeOfc.Field(i).Name)
			case ldap.ScopeWholeSubtree:
				result += fmt.Sprintf(" %s='sub'", typeOfc.Field(i).Name)
			}
		default:
			result += fmt.Sprintf(" %s='%v'", typeOfc.Field(i).Name, v.Field(i).Interface())
		}
	}

	return result[1:]
}

func (l *LDAP) connect(verboseLevel int, instance string) error {
	l.Mu.Lock()
	defer l.Mu.Unlock()

	var (
		retryLimit   = 0
		ldapCounter  = 0
		err          error
		certificates []tls.Certificate
	)

	for {
		if retryLimit > maxRetries {
			return fmt.Errorf("could not connect to any LDAP servers")
		}

		if ldapCounter > len(l.ServerURIs)-1 {
			ldapCounter = 0
		}
		if verboseLevel == logLevelDebug {
			DebugLogger.Printf("guid=\"%s\" %s connecting\n", instance, l.ServerURIs[ldapCounter])
		}
		l.LDAPConn, err = ldap.DialURL(l.ServerURIs[ldapCounter])
		if err != nil {
			ldapCounter += 1
			retryLimit += 1
			continue
		} else {
			if verboseLevel >= logLevelInfo {
				InfoLogger.Printf("%s connected\n", l.ServerURIs[ldapCounter])
			}
		}

		if l.SASLExternal {
			// Certificates are not needed with ldapi//
			if l.TLSClientCert != "" && l.TLSClientKey != "" {
				cert, err := tls.LoadX509KeyPair(l.TLSClientCert, l.TLSClientKey)
				if err != nil {
					ErrorLogger.Fatal(err)
				}
				certificates = []tls.Certificate{cert}
			}
		}

		if l.StartTLS {
			// Load CA chain
			caCert, err := ioutil.ReadFile(l.TLSCAFile)
			if err != nil {
				return err
			}
			caCertPool := x509.NewCertPool()
			caCertPool.AppendCertsFromPEM(caCert)

			u, err := url.Parse(l.ServerURIs[ldapCounter])
			if err != nil {
				return err
			}
			host, _, err := net.SplitHostPort(u.Host)
			if err != nil {
				return err
			}

			tlsConfig := &tls.Config{
				Certificates:       certificates,
				RootCAs:            caCertPool,
				InsecureSkipVerify: l.TLSSkipVerify,
				ServerName:         host,
			}

			err = l.LDAPConn.StartTLS(tlsConfig)
			if err != nil {
				ErrorLogger.Println(err)
				l.LDAPConn.Close()
				ldapCounter += 1
				retryLimit += 1
				continue
			} else {
				if verboseLevel == logLevelDebug {
					DebugLogger.Printf("guid=\"%s\" STARTTLS\n", instance)
				}
			}
		}
		break
	}

	return nil
}

func (l *LDAP) bind(verboseLevel int, instance string) {
	l.Mu.Lock()
	defer l.Mu.Unlock()

	var err error

	if l.SASLExternal {
		err = l.LDAPConn.ExternalBind()
		if err != nil {
			ErrorLogger.Println(err)
		} else {
			if verboseLevel == logLevelDebug {
				DebugLogger.Printf("guid=\"%s\" SASL/EXTERNAL\n", instance)
			}
		}
	} else {
		err = l.LDAPConn.Bind(l.BindDN, l.BindPW)
		if err != nil {
			ErrorLogger.Println(err)
		} else {
			if verboseLevel == logLevelDebug {
				DebugLogger.Printf("guid=\"%s\" simple bind\n", instance)
			}
		}
	}
}

func (l *LDAP) search(recipient string, verboseLevel int, instance string) (string, error) {
	l.Mu.Lock()
	defer l.Mu.Unlock()

	if strings.Contains(l.Filter, "%s") {
		filter := strings.ReplaceAll(l.Filter, "%s", recipient)

		if verboseLevel == logLevelDebug {
			DebugLogger.Printf("guid=\"%s\" Request: %s\n", instance, filter)
		}
		searchRequest := ldap.NewSearchRequest(
			l.BaseDN, l.Scope, ldap.NeverDerefAliases, 0, 0, false, filter, l.ResultAttr,
			nil,
		)

		searchResult, err := l.LDAPConn.Search(searchRequest)
		if err != nil {
			return "", err
		}

		for _, entry := range searchResult.Entries {
			result := entry.GetAttributeValue(l.ResultAttr[0])
			if verboseLevel == logLevelDebug {
				DebugLogger.Printf("guid=\"%s\" Response: %s=%v\n", instance, l.ResultAttr[0], result)
			}
			return result, nil
		}
	} else {
		ErrorLogger.Printf("LDAP filter does not contain '%%s' macro!\n")
	}

	return "", nil
}
