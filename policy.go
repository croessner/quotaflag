/*
quotaflag
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"fmt"
)

// Defaults
const deferText = "DEFER Service temporarily not available"
const rejectText = "REJECT User mailbox is over quota"

type Policy struct {
	policyRequest map[string]string

	guid      string
	quotaFlag bool
}

func (p *Policy) IsOverQuota(cfg *CmdLineConfig) error {
	var err error
	var ok bool
	var request string

	p.quotaFlag = false

	if request, ok = p.policyRequest["request"]; ok {
		if request == "smtpd_access_policy" {
			if protoState, ok := p.policyRequest["protocol_state"]; ok {
				if protoState == "RCPT" {
					if recipient, ok := p.policyRequest["recipient"]; ok {
						if len(recipient) > 0 {
							if cfg.UseHTTP {
								p.quotaFlag, err = getQuotaFlagHttp(recipient, cfg.HttpAddress, cfg.VerboseLevel, p.guid)
							} else if cfg.UseLDAP {
								p.quotaFlag, err = getQuotaFlagLdap(recipient, cfg.VerboseLevel, p.guid)
							} else {
								return fmt.Errorf("need HTTP or LDAP")
							}
						}
					}
				}
			}
		}
	}

	return err
}
